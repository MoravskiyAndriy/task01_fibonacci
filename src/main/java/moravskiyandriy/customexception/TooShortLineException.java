package moravskiyandriy.customexception;

public class TooShortLineException extends Exception {
    public TooShortLineException(String errorMessage){
        super(errorMessage);
    }
}

