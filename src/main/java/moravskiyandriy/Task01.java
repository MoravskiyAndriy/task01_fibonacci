package moravskiyandriy;

/**
 * <h1>Task01_Fibonacci</h1>
 * The Task01_Fibonacci program implements an application, which
 * consists of two parts: first part simply displays numbers from
 * defined interval to the standard output and print sums of odd
 * and even numbers respectively.
 * Second part seek the biggest odd and even numbers from the set
 * and show persentages of odd and even numbers in it.
 * <p>
 *
 * @author Andriy Moravskiy
 * @version 1.0
 * @since 2019-08-24
 */

import moravskiyandriy.customexception.TooShortLineException;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

public class Task01 {
    private static final int TOO_SHORT_LINE = 5;

    /**
     * This is the main method which makes use of printNumbers,
     * printSum and fibonacciNumbers methods.
     *
     * @param args Unused.
     * @throws IOException On input error.
     * @see IOException
     */
    public static void main(final String[] args) throws IOException {
        Scanner in = new Scanner(System.in);
        System.out.println("Enter the interval ");
        String[] startEnd = in.nextLine().split(" ");
        int start = Integer.parseInt(startEnd[0]);
        int end = Integer.parseInt(startEnd[1]);
        printNumbers(start, end);
        printSum(start, end);
        fibonacciNumbers();
    }

    static void lineChecker(List<Integer> numberList) throws TooShortLineException {
        if(numberList.size()<TOO_SHORT_LINE){
            throw new TooShortLineException("Line of numbers will be too short!");
        }
    }


    /**
     * This method is used to print odd and even integers from the interval.
     * (even in the reversed order).
     *
     * @param start This is the first paramter to printNumbers method
     * @param end   This is the second parameter to printNumbers method
     */
    static void printNumbers(final int start, final int end){
        List<Integer> listOfOddNumbers = new ArrayList<Integer>();
        for (int i = start; i <= end; i++) {
            if (i % 2 != 0) {
                listOfOddNumbers.add(i);
            }
        }
        List<Integer> listOfEvenNumbers = new ArrayList<Integer>();
        for (int i = start; i <= end; i++) {
            if (i % 2 == 0) {
                listOfEvenNumbers.add(i);
            }
        }
        try{
            lineChecker(listOfOddNumbers);
        }catch (TooShortLineException e){
            e.printStackTrace();
        }
        try{
            lineChecker(listOfEvenNumbers);
        }catch (TooShortLineException e){
            e.printStackTrace();
        }
        for (int item : listOfOddNumbers) {
            System.out.print(item + " ");
        }
        System.out.println();
        Collections.reverse(listOfEvenNumbers);
        for (int item : listOfEvenNumbers) {
            System.out.print(item + " ");
        }
        System.out.println("\n");
    }

    /**
     * This method is used to print sums of
     * odd and even integers from the interval.
     *
     * @param start This is the first paramter to printSum method
     * @param end   This is the second parameter to printSum method
     */
    static void printSum(final int start, final int end) {
        int oddSum = 0;
        int evenSum = 0;
        for (int i = start; i <= end; i++) {
            if (i % 2 != 0) {
                oddSum += i;
            } else {
                evenSum += i;
            }
        }
        System.out.println("Sum of odd numbers= " + oddSum
                + ". Sum of even numbers= " + evenSum);
    }


    /**
     * This method is used to print the biggest
     * odd and even Fibonacci numbers from the set.
     * (Size of the set is defined by the user).
     * Also it prints the percentage of odd and even numbers from the set.
     */
    static void fibonacciNumbers() {
        System.out.println("input size of set for Fibonacci numbers (>=2)");
        Scanner in = new Scanner(System.in);
        int size = in.nextInt();
        int f1 = 1;
        int f2 = 0;
        int quantityOfOddNumbers = 0;
        int quantityOfEvenNumbers = 0;
        ArrayList<Integer> fibonacciSet = new ArrayList<Integer>();
        fibonacciSet.add(1);
        fibonacciSet.add(1);
        for (int i = 2; i < size; i++) {
            fibonacciSet.add(fibonacciSet.get(i - 1) + fibonacciSet.get(i - 2));
        }
        for (int i = 0; i < size; i++) {
            if (fibonacciSet.get(i) % 2 != 0) {
                quantityOfOddNumbers += 1;
                f1 = fibonacciSet.get(i);
            }
            if (fibonacciSet.get(i) % 2 == 0) {
                quantityOfEvenNumbers += 1;
                f2 = fibonacciSet.get(i);
            }

        }
        double oddPercentage = ((double) quantityOfOddNumbers)
                / (quantityOfOddNumbers + quantityOfEvenNumbers);
        System.out.println("Biggest odd number= " + f1
                + ". Biggest even number= " + f2 + "\n");
        System.out.println("Persentage of odd Fibonacci Numbers= "
                + new DecimalFormat("##.##").format(oddPercentage)
                + ". Persentage of even Fibonacci Numbers= "
                + new DecimalFormat("##.##").format(1 - oddPercentage));
    }


}
